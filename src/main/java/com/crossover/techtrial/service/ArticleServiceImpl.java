package com.crossover.techtrial.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import com.crossover.techtrial.model.Article;
import com.crossover.techtrial.repository.ArticleRepository;
import org.springframework.data.domain.Pageable;


@Service
public class ArticleServiceImpl implements ArticleService {

  @Autowired
  ArticleRepository articleRepository;

  public Article save(Article article) {
    return articleRepository.save(article);
  }

  public Article findById(Long id) {
    return articleRepository.findById(id).orElse(null);
  }

  public void delete(Long id) {
    articleRepository.deleteById(id);
  }


  public List<Article> search(String search,Pageable pageable){
    return articleRepository
        .findByTitleContainsOrContentContainsAllIgnoreCase(search, search,pageable);
  }

}