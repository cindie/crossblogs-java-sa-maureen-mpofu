package com.crossover.techtrial.controller;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import com.crossover.techtrial.model.Article;
import sun.plugin2.message.Message;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class ArticleControllerTest {

  @Autowired
  private TestRestTemplate template;

  @Before
  public void setup() throws Exception {

  }

  @Test
  public void testArticleShouldBeCreated() throws Exception {
    HttpEntity<Object> article = getHttpEntity(
        "{\"email\": \"user1@gmail.com\", \"title\": \"hello\" }");
    ResponseEntity<Article> resultAsset = template.postForEntity("/articles", article,
        Article.class);
    Assert.assertNotNull(resultAsset.getBody().getId());
  }

  @Test
  public void testArticleShouldReturnArticleIfExists() throws Exception {
    ResponseEntity<Article> resultAsset = template.getForEntity("/articles/1",
            Article.class);
    Assert.assertNotNull(resultAsset.getBody());
  }

  @Test
  public void testArticleShouldUpdateArticleIfExists() throws Exception {
    HttpEntity<Object> article = getHttpEntity(
            "{\"email\": \"user4@gmail.com\", \"title\": \"hello everyone\" }");
    ResponseEntity<Article> resultAsset= template.exchange("/articles/1", HttpMethod.PUT, article, Article.class);
    Assert.assertEquals("hello everyone",resultAsset.getBody().getTitle());
  }

  @Test
  public void testArticleShouldBeDeletedIfExists() throws Exception {
    ResponseEntity resultAsset= template.exchange("/articles/1", HttpMethod.DELETE,
            null, Object.class);
    Assert.assertEquals(HttpStatus.OK,resultAsset.getStatusCode());
  }

  @Test
  public void testArticleShouldReturnArticlesWithMatchingTitleAndContentWhichContainsTextSpecified() throws Exception {
    ResponseEntity<Article[]> resultAsset= template.getForEntity("/articles/search?text=How to boil an egg",
            Article[].class);
    System.out.print(resultAsset.getBody());
    Assert.assertNotNull(resultAsset.getBody());
  }

  private HttpEntity<Object> getHttpEntity(Object body) {
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    return new HttpEntity<Object>(body, headers);
  }
}
